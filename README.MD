LUNR Game - libunr Unreal Game Package, a remake of Unreal in libunr.

	LUNR Game is a collection of packages that intend to re-create Unreal from scratch, with the intention of utilizing 
	libunr's technological improvements over the original Unreal Engine. It is broken up into a selection of Packages 
	that each implement a different level of the game. Additionally, some extra packages such as gamemodes are 
	included as part of this repository.
	
	Packages:
	
		-LUNR_Base
			
			Implements whatever native(if any) and UnrealScript framework that creates the foundation for a game that 
			plays like Unreal. e.g. the game itself is defined by this package. Also implements whatever functionality 
			nessecary to seamlessly interact with legacy content (Unreal 1 mods) that may be used with the new 
			game foundation.
			
		-LUNR_UnrealI
		
			Remake of Unreal in libunr. The scope of this package includes Unreal I, Unreal RTNP (Gold), as well 
			as remastering/allocation of contents and ideas from the Alphas and Betas for Unreal.
			
			-LUNR_UnrealI_Assets
			
				Contains any non-code content (such as models and textures) for LUNR_UnrealI_Classes 
				(See next package). Provided as a seperate package so that code updates may be performed 
				independently of Assets. (Download optimization)
				
			-LUNR_UnrealI_Classes
			
				Contains all the classes/objects to implement LUNR_UnrealI.
				
		-LUNR_UT
		
			Remake of UT99 in libunr, as well as optional gameplay overhauls, content, and gamemodes inspired 
			by features from later UTs (most notably UT4).
			
			-LUNR_UT_Assets
			
			-LUNR_UT_Classes
			
		-LUNR_RLA (Rogue-like ADventure)
		
			Configurable framework for LUNR_Base to implement a Roguelike RPG gamemode, with a high emphasis 
			on procedural generation, exploration, and teamwork. May use whatever content modules the 
			admin provides to allow seamless integration of new content on the fly as well as variety.
			
		-LUNR_TacticalCoop
		
			Gamemode that implements many features that allow for a more realistic, as well as tactical 
			experience. Inspired by RLCoop, ArmA, UT99 Infiltration, DayZ, this mod contains features 
			such as recoil, non-fixed weapon movement, freelook, stanima, medical system, inventory, 
			ballistics, etc., all of which are optional.